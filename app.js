window.addEventListener('load', () => {
    let long;
    let lat;
    const temperatureDegreee = document.querySelector('.temperature-degree');
    const locationTimezone = document.querySelector('.location-timezone');
    const temperatureDescription = document.querySelector('.temperature-description');
    const weatherIcon = document.querySelector('.weather-icon');
    const temperatureSection = document.querySelector('.temperature');
    const temperatureSpan = document.querySelector('.temperature span');
    const windSection = document.querySelector('.wind');
    const humiditySection = document.querySelector('.humidity');

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;
            const api = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&units=imperial&appid=80ee88fd55c12cc56d88d06716087375`;

            fetchWeatherData(api);
        });
    } else {
        locationTimezone.textContent = "Geolocation not available";
    }

    temperatureSection.addEventListener('click', toggleTemperatureUnit);

    const searchBox = document.querySelector('.search input');
    const searchBtn = document.querySelector('.search button');

    searchBtn.addEventListener('click', () => {
        const city = searchBox.value;
        const api = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&appid=80ee88fd55c12cc56d88d06716087375`;

        fetchWeatherData(api);
    });

    function fetchWeatherData(api) {
        fetch(api)
            .then(response => response.json())
            .then(data => {
                const temp = Math.round(data.main.temp);
                const location = data.sys.country + '/' + data.name;
                const description = data.weather[0].description;
                const wind=data.wind.speed+"km/h";
                const humidity=data.main.humidity+"%";

                temperatureDegreee.textContent = temp;
                locationTimezone.textContent = location;
                temperatureDescription.textContent = description;
                windSection.textContent=wind;
                humiditySection.textContent=humidity;

                updateImage(data.weather[0].main);
            });
    }

    function updateImage(description) {
        switch (description) {
            case "Clouds":
                weatherIcon.src = "images/clouds.png";
                break;
            case "Clear":
                weatherIcon.src = "images/clear.png";
                break;
            case "Rain":
                weatherIcon.src = "images/rain.png";
                break;
            case "Drizzle":
                weatherIcon.src = "images/drizzle.png";
                break;
            case "Mist":
                weatherIcon.src = "images/mist.png";
                break;
            default:
                weatherIcon.src = "images/clear.png";
                break;
        }
    }

    function toggleTemperatureUnit() {
        const currentTemperature = parseFloat(temperatureDegreee.textContent);

        if (temperatureSpan.textContent === "°F") {
            temperatureSpan.textContent = "°C";
            temperatureDegreee.textContent =Math.round( ((currentTemperature - 32) * 5/9));
        } else {
            temperatureSpan.textContent = "°F";
            temperatureDegreee.textContent =Math.round( ((currentTemperature * 9/5) + 32));
        }
    }
});
